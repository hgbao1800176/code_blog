<?php
require('../include/Init.php');
if($data_user['Level'] != 1){
    echo '<meta http-equiv="refresh" content="0;url=/">';
    die();
}
$title = "Trang chủ";
    $CASE = $_GET['page'];
    switch ($CASE) {
        case '': // trang dasboard
            include '../page/layout/Header.php';
            include 'page/Main.php';
            include ('../page/layout/Footer.php');
            break;
        case 'add':
            $title = "Bài viết mới";
            include '../page/layout/Header.php';
            include 'page/blog/Add.php';
            include ('../page/layout/Footer.php');
            break;
        case 'edit':
            $title = "Bài viết mới";
            include '../page/layout/Header.php';
            include 'page/blog/Edit.php';
            include ('../page/layout/Footer.php');
            break;    
        case 'blog':
            $title = "Quản lý bài viết";
            include '../page/layout/Header.php';
            include 'page/blog/index.php';
            include ('../page/layout/Footer.php');
            break;
        case 'category':
            $title = "Danh mục";
            include '../page/layout/Header.php';
            include 'page/blog/category.php';
            include ('../page/layout/Footer.php');
            break;
        case 'list':       
            include '../page/layout/Header.php';
            include 'page/blog/List.php';
            include ('../page/layout/Footer.php');
            break;
    
        default: // trang chu
            include '../page/layout/Header.php';
            include '../page/layout/NotFound.php';
            break;
    }
    
    


