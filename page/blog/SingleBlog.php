<?php
$id = $_GET['id'];
$result = $db->fetch_assoc( "SELECT * FROM `post` WHERE `Id`='$id'",1);
// $user_post = $db->fetch_assoc("SELECT * FROM `accounts` WHERE `Id`= ".$result['User_id']." ",1);

$cate = $db->fetch_assoc("SELECT * FROM `category` WHERE `Id`=".$result['Category_id']." ",1);

$cmt = $db->fetch_assoc("SELECT * FROM `comment` WHERE `Post_id` = ".$result['Id']." ",0);
// $user_cmt = $db->fetch_assoc("SELECT * FROM `accounts` WHERE `Id` = ".$cmt['User_id']."",0);
// echo $user_cmt['Username'];

?>
<script>
        
		function Cmt(){
		   var content = $('#content').val();
		   var post_id = $('#post_id').val();
			   $.ajax({
				   url : "page/Authenticate/Action.php?func=cmt",
				   type : "post",
				   dataType:"text",
				   data : {
					   content,
					   post_id
					   
				   },
				   success : function (result){					  
					   $('#result').html(result);
					   $('#Cmt').val("");					   
					   
				   }
			   });
		   }
   </script>
	<!-- section main content -->
	<section class="main-content mt-3">
		<div class="container-xl">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Inspiration</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$result['Title']?></li>
                </ol>
            </nav>

			<div class="row gy-4">

				<div class="col-lg-8">
					<!-- post single -->
                    <div class="post post-single">
						<!-- post header -->
						<div class="post-header">
							<h1 class="title mt-0 mb-3"><?=$result['Title']?></h1>
							<ul class="meta list-inline mb-0">
								<li class="list-inline-item"><a href="#"><img src="../../../assets/images/other/author-sm.png" class="author" alt="author"/><?=$result['Username']?></a></li>
								<li class="list-inline-item"><a href="#"><?=$cate['Name']?></a></li>
								<li id="post_id" value="<?=$result['Id']?>" class="list-inline-item"><?=$result['Time']?> </li>
								
							</ul>
						</div>
						<!-- featured image -->
						
						<!-- post content -->
						<?=$result['Content']?>
						<!-- post bottom section -->
						<div class="post-bottom">
							<div class="row d-flex align-items-center">
								<div class="col-md-6 col-12 text-center text-md-start">
									<!-- tags -->
									<a href="#" class="tag">#Trending</a>
									<a href="#" class="tag">#Video</a>
									<a href="#" class="tag">#Featured</a>
								</div>
								<div class="col-md-6 col-12">
									<!-- social icons -->
									<ul class="social-icons list-unstyled list-inline mb-0 float-md-end">
										<li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fab fa-pinterest"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="far fa-envelope"></i></a></li>
									</ul>
								</div>
							</div>
						</div>

                    </div>

					<div class="spacer" data-height="50"></div>

					

					<div class="spacer" data-height="50"></div>
		   				<?php $sum_cmt = $db->num_rows("SELECT `Content` FROM `comment` WHERE `Post_id`=".$result['Id'].""); ?>
					<!-- section header -->
					<div class="section-header">
						<h3 class="section-title">Comments (<?=$sum_cmt?>)</h3>
						<img src="../../../assets/images/wave.svg" class="wave" alt="wave" />
					</div>
					<!-- post comments -->
					<div class="comments bordered padding-30 rounded">

						<ul class="comments">
							<!-- comment item -->
						<?php			
						foreach($cmt as $row){														
						echo '
          
							<li class="comment rounded">
								<div class="thumb">
									<img src="../../../assets/images/other/comment-1.jpg" style="max-width: 60%;" alt="Avatar" />
								</div>
								<div class="details">
									
										<h4 class="name"><a href="info" target="_blank">'.$row['Username'].'</a></h4>
									<span class="date">'.$row['Time'].'</span>
									<p>'.$row['Content'].'</p>
									<a href="#" class="btn btn-default btn-sm">Reply</a>
								</div>
							</li> '; }?>
							<!-- comment item -->
							<!-- <li class="comment child rounded">
								<div class="thumb">
									<img src="../../../assets/images/other/comment-2.png" alt="John Doe" />
								</div>
								<div class="details">
									<h4 class="name"><a href="#">Helen Doe</a></h4>
									<span class="date">Jan 08, 2021 14:41 pm</span>
									<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>
									<a href="#" class="btn btn-default btn-sm">Reply</a>
								</div>
							</li> -->
							<!-- comment item -->
							
						</ul>
					</div>

					<div class="spacer" data-height="50"></div>

					<!-- section header -->
					<div class="section-header">
						<h3 class="section-title"> Comment</h3>
						<img src="../../../assets/images/wave.svg" class="wave" alt="wave" />
					</div>
					<!-- comment form -->
					<div class="comment-form rounded bordered padding-30">

						
				
							<div class="messages"></div>
							
							<div class="row">

								<div class="column col-md-12">
									<!-- Comment textarea -->
									<div class="form-group">
										<textarea  id="content" class="form-control" rows="4" placeholder="Your comment here..." ></textarea>
									</div>
								</div>

								
						
							</div>
	
							<button  id="submit"  class="btn btn-default" onClick="Cmt()">Bình Luận</button><!-- Submit Button -->
							<div id="result">  </div> 
						
					</div>
                </div>

				<div class="col-lg-4">

					<!-- sidebar -->
					<div class="sidebar">
						<!-- widget about -->
						<div class="widget rounded">
							<div class="widget-about data-bg-image text-center" data-bg-image="../../../assets/images/map-bg.png">
								<img src="../../../assets/images/logo.svg" alt="logo" class="mb-4" />
								<p class="mb-4">Đây là lời giới thiệu</p>
								<ul class="social-icons list-unstyled list-inline mb-0">
									<li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li class="list-inline-item"><a href="#"><i class="fab fa-pinterest"></i></a></li>
									<li class="list-inline-item"><a href="#"><i class="fab fa-medium"></i></a></li>
									<li class="list-inline-item"><a href="#"><i class="fab fa-youtube"></i></a></li>
								</ul>
							</div>
						</div>

						<!-- widget popular posts -->
						<div class="widget rounded">
							<div class="widget-header text-center">
								<h3 class="widget-title">Bài viết liên quan</h3>
								<img src="../../../assets/images/wave.svg" class="wave" alt="wave" />
							</div>
							<div class="widget-content">
								<!-- post -->
								<?php
								$post = $db->fetch_assoc("SELECT * FROM `post` ",0);	
								$i = 0;
								foreach($post as $row){
									if (++$i == 4) break;
								echo '	
								<div class="post post-list-sm circle">
									<div class="thumb circle">
										
										<a href="blog-single.html">
											<div class="inner">
												<img src="../../../assets/images/posts/tabs-1.jpg" alt="post-title" />
											</div>
										</a>
									</div>
									<div class="details clearfix">
										<h6 class="post-title my-0"><a href="blog-single.html">'.$row['Title'].'</a></h6>
										<ul class="meta list-inline mt-1 mb-0">
											<li class="list-inline-item">'.$row['Time'].'</li>
										</ul>
									</div>
								</div> ';}?>
								<!-- post -->
								
							</div>		
						</div>

						<!-- widget categories -->
						<div class="widget rounded">
							<div class="widget-header text-center">
								<h3 class="widget-title">Bài viết</h3>
								<img src="../../../assets/images/wave.svg" class="wave" alt="wave" />
							</div>
							<div class="widget-content">
								<ul class="list">
									<li><a href="#">Lifestyle</a><span>(5)</span></li>
									<li><a href="#">Inspiration</a><span>(2)</span></li>
									<li><a href="#">Fashion</a><span>(4)</span></li>
									<li><a href="#">Politic</a><span>(1)</span></li>
									<li><a href="#">Trending</a><span>(7)</span></li>
									<li><a href="#">Culture</a><span>(3)</span></li>
								</ul>
							</div>
							
						</div>

						<!-- widget newsletter -->
						

						<!-- widget post carousel -->
						<div class="widget rounded">
							<div class="widget-header text-center">
								<h3 class="widget-title">Nổi bật</h3>
								<img src="../../../assets/images/wave.svg" class="wave" alt="wave" />
							</div>
							<div class="widget-content">
								<div class="post-carousel-widget">
									<!-- post -->
									<?php
								$post = $db->fetch_assoc("SELECT * FROM `post` ",0);	
								$i = 0;
								foreach($post as $row){
									if (++$i == 4) break;
									echo '
									<div class="post post-carousel">
										<div class="thumb rounded">
											<a href="category.html" class="category-badge position-absolute">How to</a>
											<a href="blog-single.html">
												<div class="inner">
													<img src="../../../assets/images/widgets/widget-carousel-1.jpg" alt="post-title" />
												</div>
											</a>
										</div>
										<h5 class="post-title mb-0 mt-4"><a href="blog-single.html">'.$row['Title'].'</a></h5>
										<ul class="meta list-inline mt-2 mb-0">
											<li class="list-inline-item"><a href="#">'.$row['Username'].'</a></li>
											<li class="list-inline-item">'.$row['Time'].'</li>
										</ul>
									</div> ';} ?>
									<!-- post -->
									
									<!-- post -->
									
								</div>
								<!-- carousel arrows -->
								<div class="slick-arrows-bot">
									<button type="button" data-role="none" class="carousel-botNav-prev slick-custom-buttons" aria-label="Previous"><i class="icon-arrow-left"></i></button>
									<button type="button" data-role="none" class="carousel-botNav-next slick-custom-buttons" aria-label="Next"><i class="icon-arrow-right"></i></button>
								</div>
							</div>		
						</div>

						<!-- widget advertisement -->
						<div class="widget no-container rounded text-md-center">
							<span class="ads-title">- Sponsored Ad -</span>
							<a href="#" class="widget-ads">
								<img src="../../../assets/images/ads/ad-360.png" alt="Advertisement" />	
							</a>
						</div>

						<!-- widget tags -->
						<div class="widget rounded">
							<div class="widget-header text-center">
								<h3 class="widget-title">Tag Clouds</h3>
								<img src="../../../assets/images/wave.svg" class="wave" alt="wave" />
							</div>
							<div class="widget-content">
								<a href="#" class="tag">#Trending</a>
								<a href="#" class="tag">#Video</a>
								<a href="#" class="tag">#Featured</a>
								<a href="#" class="tag">#Gallery</a>
								<a href="#" class="tag">#Celebrities</a>
							</div>		
						</div>

					</div>

				</div>

			</div>

		</div>
	</section>
